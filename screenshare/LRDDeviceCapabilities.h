//
//  LRDDeviceCapabilities.h
//  LRD
//
//  Created by Alan Morris on 13/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    iPhoneNonRetina = 1,
    iPhoneRetina3_5 = 2,
    iPhoneRetina4 = 3,
    iPadNonRetina = 11,
    iPadRetina = 12
} DeviceResolution;

@interface LRDDeviceCapabilities : NSObject

-(DeviceResolution)currentDeviceScreenType;

@end
