//
//  main.m
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SSAppDelegate class]));
    }
}
