//
//  SSImageSelectViewController.m
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import "SSImageSelectViewController.h"

@interface SSImageSelectViewController ()

@end

@implementation SSImageSelectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Setting the corner radius of the view and the ImageView contained within. MasksToBounds ensures the image view is also rounded.
    self.view.layer.cornerRadius = 12;
    _popupBackground.layer.cornerRadius = 12;
    _popupBackground.layer.masksToBounds = YES;
    
    // Setting the content and font specifics of the title label of the image selection pop up.
    _titleLabel.font = [UIFont fontWithName:@"Lora-Italic" size:16.0f];
    _titleLabel.text = @"Please select your screenshot source from the options below";
}

-(IBAction)cameraBtnPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cameraBtnPressed:)])
    {
        [self.delegate cameraBtnPressed:self];
    }
}

-(IBAction)cameraRollBtnPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cameraRollBtnPressed:)])
    {
        [self.delegate cameraRollBtnPressed:self];
    }
}

-(IBAction)dropboxBtnPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(dropboxBtnPressed:)])
    {
        [self.delegate dropboxBtnPressed:self];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
