//
//  LRDDeviceCapabilities.m
//  LRD
//
//  Created by Alan Morris on 13/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import "LRDDeviceCapabilities.h"

@implementation LRDDeviceCapabilities


-(DeviceResolution)currentDeviceScreenType
{
    DeviceResolution displayType;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)])
        {
            CGSize device = [[UIScreen mainScreen] bounds].size;
            CGFloat scale = [UIScreen mainScreen].scale;
            device = CGSizeMake(device.width * scale, device.height * scale);
            
            if(device.height == 1136)
            {
                //Device is an iPhone 5
                displayType = iPhoneRetina4;
            }
            else if(device.height == 960)
            {
                //Device is an iPhone 4 or iPhone 4S
                displayType = iPhoneRetina3_5;
            }
        }
        else
        {
            //Device is an iPhone, iPhone 3G, or iPhone 3GS
            displayType = iPhoneNonRetina;
        }
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)])
        {
            CGSize device = [[UIScreen mainScreen] bounds].size;
            CGFloat scale = [UIScreen mainScreen].scale;
            device = CGSizeMake(device.width * scale, device.height * scale);
            
            if(device.height == 2048)
            {
                //Device is an iPad 3
                displayType = iPadRetina;
            }
        }
        else
        {
            //Device is an iPad or iPad 2
            displayType = iPadNonRetina;
        }

    }

    return displayType;
}

@end
