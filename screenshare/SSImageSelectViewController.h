//
//  SSImageSelectViewController.h
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol ImageSelectionPopupDelegate;

@interface SSImageSelectViewController : UIViewController

@property (assign, nonatomic) id <ImageSelectionPopupDelegate>delegate;

@property (nonatomic, weak) IBOutlet UIButton       *cameraBtn;
@property (nonatomic, weak) IBOutlet UIButton       *cameraRollBtn;
@property (nonatomic, weak) IBOutlet UIButton       *dropboxBtn;
@property (nonatomic, weak) IBOutlet UILabel        *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView    *popupBackground;


@end

@protocol ImageSelectionPopupDelegate<NSObject>
@optional

-(void)cameraBtnPressed:(SSImageSelectViewController *)imageSelector;
-(void)cameraRollBtnPressed:(SSImageSelectViewController *)imageSelector;
-(void)dropboxBtnPressed:(SSImageSelectViewController *)imageSelector;

@end
