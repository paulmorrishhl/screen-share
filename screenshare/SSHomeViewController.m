//
//  SSViewController.m
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import "SSHomeViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SSImageSelectViewController.h"


@interface SSHomeViewController ()

@end

@implementation SSHomeViewController


#pragma mark - View Loading Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Create custom navigation title
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(96.0f, 4.0f, 320.0f, 40.0f)];
    navLabel.text = @"Screen Share";
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont fontWithName:@"Overlock-BlackItalic" size:(26.0f)];
    navLabel.shadowColor = [UIColor blackColor];
    navLabel.shadowOffset = CGSizeMake (1.0f,1.0f);
    navLabel.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = navLabel;
    
}

-(void) anAlertWithTitle:(NSString *)title withMessage:(NSString *)message withCancelTitle:(NSString *)cancel andsecondButtonTitle:(NSString *)secondButton
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                            message:message
                                            delegate:self
                                            cancelButtonTitle:cancel
                                            otherButtonTitles:secondButton, nil];
    [alert show];
}

#pragma mark - Camera & Camera Roll Delegate Methods

- (void)presentPickerWithSourceType:(UIImagePickerControllerSourceType)pickerSourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:pickerSourceType])
    {
        //Create and initialise the picker, using the source type passed in from the calling method
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = pickerSourceType;
        imagePicker.mediaTypes = [NSArray arrayWithObjects: (NSString *) kUTTypeImage, nil]; // images only, could add kUTTypeMovies as well if required.
        imagePicker.allowsEditing = NO;
        
        //Display the picker to the user and then release the allocated memory
        [self presentModalViewController:imagePicker animated:YES];
    }
    
    else
        
    {
        [self anAlertWithTitle:@"An Error Occurred" withMessage:@"The device you are using doesn't seem to have an available camera.\n\nPlease select a screenshot from the device camera roll or upload one using the Dropbox option." withCancelTitle:@"Dismiss" andsecondButtonTitle:nil];
    }
}

// image picker controller delegate method, called when user has finished taking or selecting an image.
-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // casting the type of media into an NSString
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    [self dismissModalViewControllerAnimated:YES];
    
    // Comparing the media type and casting it into the ImageView if it is an image.
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
		_cameraHolder.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// stick some code here if video is supported
	}
	
}

// delegate method to produce an error if an issue occurred.
- (void) image: (UIImage *) image finishedSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save Failed"
                              message: @"Failed to save requested image"
                              delegate: nil
                              cancelButtonTitle:@"Dismiss"
                              otherButtonTitles:nil];
        [alert show];
    }
}

// delegate method to dismiss the camera or camera roll if cancel is pressed.
- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Segue Preperation Method

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark - Custom Button Methods

-(IBAction)addBtnPressed:(id)sender
{
    // generating a random number between 1 & 4. Each number relates to a specific animation type to display the modal image select view.
    int randomAnimation = (arc4random() % 4) + 1;
    imageSelector = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageSelectPopup"];
    imageSelector.delegate = self;
    [self presentPopupViewController:imageSelector animationType:randomAnimation];
}

-(IBAction)savedBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"SavedScreensSegue" sender:self];
}

#pragma mark - Modal Popup Delegate Methods

-(void)cameraBtnPressed:(SSImageSelectViewController *)imageSelector
{
    NSLog(@"CAMERA BUTTON PRESSED");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    [self presentPickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
    
}

-(void)cameraRollBtnPressed:(SSImageSelectViewController *)imageSelector
{
    NSLog(@"CAMERA ROLL BUTTON PRESSED");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    [self presentPickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

-(void)dropboxBtnPressed:(SSImageSelectViewController *)imageSelector
{
    NSLog(@"DROPBOX BUTTON PRESSED");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
