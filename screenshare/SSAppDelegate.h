//
//  SSAppDelegate.h
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
