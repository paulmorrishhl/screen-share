//
//  SSSavedViewController.m
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import "SSSavedViewController.h"

@interface SSSavedViewController ()

@end

@implementation SSSavedViewController

-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Creating a custom navigation title for the view
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(96.0f, 4.0f, 128.0f, 40.0f)];
    navLabel.text = @"Saved Screens";
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont fontWithName:@"Overlock-BlackItalic" size:(26.0f)];
    navLabel.shadowColor = [UIColor blackColor];
    navLabel.shadowOffset = CGSizeMake (1.0f,1.0f);
    navLabel.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = navLabel;
    
    // Creating an a custom button with an image to use as a bar button item on nav bar.
    UIImage *image = [UIImage imageNamed:@"backButton.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
