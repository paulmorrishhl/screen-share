//
//  SSViewController.h
//  screenshare
//
//  Created by Paul Morris on 12/09/2012.
//  Copyright (c) 2012 Little Red Door Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIViewController+MJPopupViewController.h"
#import "SSImageSelectViewController.h"

@interface SSHomeViewController : UIViewController <ImageSelectionPopupDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    SSImageSelectViewController *imageSelector;
    
}

@property (nonatomic, weak) IBOutlet    UIButton                    *addBtn;
@property (nonatomic, weak) IBOutlet    UIButton                    *savedBtn;

@property (nonatomic, retain)           UIImagePickerController     *imgPicker;
@property (nonatomic, weak) IBOutlet    UIImageView                 *cameraHolder;

-(IBAction)addBtnPressed:(id)sender;
-(IBAction)savedBtnPressed:(id)sender;

- (void)presentPickerWithSourceType:(UIImagePickerControllerSourceType)pickerSourceType;

@end
